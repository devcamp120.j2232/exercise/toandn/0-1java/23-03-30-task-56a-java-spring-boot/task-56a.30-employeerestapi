package com.devcamp.employee_rest_api.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class EmployeeRestController {
  @GetMapping("/employees")
  public ArrayList<Employee> getListEmployee() {
    //khởi tạo 3 đối tượng nhân viên (có tham số) truyền vào các tham số
    Employee employee1 = new Employee(01, "Nguyen Van", "A", 1000);
    Employee employee2 = new Employee(02, "Lai Thi", "B", 1100);
    Employee employee3 = new Employee(03, "Le Quoc", "C", 900);
    //Sử dụng phương thức toString để in 3 đối tượng trên ra console
    System.out.println("Employee1: " + employee1.toString());
    System.out.println("Employee2: " + employee2.toString());
    System.out.println("Employee3: " + employee3.toString());
    //tạo danh sách lớp employee
    ArrayList<Employee> employees = new ArrayList<>();
    employees.add(employee1);
    employees.add(employee2);
    employees.add(employee3);

    return employees;
}
}
